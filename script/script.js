let pageDefault = document.querySelector(".page-default");
let changeTheme = document.querySelector(".btn-change-theme");

function showDefaultColor() {
  pageDefault.style.backgroundColor = "#ffffff";
}
function showChangeColor() {
  pageDefault.style.backgroundColor = "#b5b7b8";
}

let changeColor;
changeTheme.addEventListener("click", () => {
  if (!changeColor) {
    localStorage.setItem("changeColor", "true");
    changeColor = true;
    showChangeColor();
  } else {
    localStorage.setItem("changeColor", "false");
    changeColor = false;
    showDefaultColor();
  }
});

if (localStorage.getItem("changeColor") === "true") {
  changeColor = true;
  showChangeColor();
} else {
  localStorage.getItem("changeColor", "false");
  changeColor = false;
  showDefaultColor();
}
